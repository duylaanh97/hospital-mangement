import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroupComponent } from './group/group.component';
import { UserComponent } from './user/user.component';
import { PermissionComponent } from './permission/permission.component';

const routes: Routes = [
  {
    path: 'group', component: GroupComponent,pathMatch: 'full'
  },
  {
    path: 'list-users', component: UserComponent
  },
  {
    path: 'permission', component: PermissionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
