import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { GroupComponent } from './group/group.component';
import { UserComponent } from './user/user.component';
import { PermissionComponent } from './permission/permission.component';
import { TableModule } from 'primeng/table';
import { MessageService } from 'primeng/api';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { ConfirmDialogModule } from 'primeng/components/confirmdialog/confirmdialog';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { MenubarModule } from 'primeng/components/menubar/menubar';
import {ButtonModule} from 'primeng/button';
import {SplitButtonModule} from 'primeng/splitbutton';

import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [GroupComponent, UserComponent, PermissionComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    TableModule,
    DialogModule,
    ConfirmDialogModule,
    DropdownModule,
    MenubarModule,
    ButtonModule,
    SplitButtonModule,
    FormsModule
  ],
  providers:[MessageService]
})
export class UserModule { }
