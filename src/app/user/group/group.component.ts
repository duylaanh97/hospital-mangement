import { Component, OnInit } from '@angular/core';
import { Group } from './model/group.model';
import { MessageService } from 'primeng/primeng';
import * as jsPDF from 'jspdf';
import { CarService } from 'src/app/demo/service/carservice';

interface Gender{
  label: string;
  value: string
}
@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css'],
  providers: [CarService]
})

export class GroupComponent implements OnInit {

  group: Group = {}

  groups: Group[];

  cols: any[];

  displayDialog: boolean;

  newGroup: boolean;

  selectedgroup: Group;

  select : boolean = false;

  genders: Gender[];

  selectGender: Gender;


  constructor(private groupService: CarService, private messageService: MessageService) { }

  ngOnInit() {
    this.groupService.getgroup().then(groups => {
      console.log(groups);
      this.groups = groups;
    });

    this.genders = [
      {label:'Nam',value:'NM'},
      {label:'Nữ',value:'NN'}
    ];

    this.cols = [
      { field: 'stt',  header: 'STT' },
      { field: 'hoTen', header: 'Họ Tên' },
      { field: 'chuyenMon', header: 'Chuyên Môn' },
      { field: 'luong', header: 'Tiền Lương (VNĐ)' },
      { field: 'gioiTinh', header: 'Giới Tính' },
      { field: 'diaChi', header: 'Địa Chỉ' },
      { field: 'soDienThoai', header: 'Số Điện Thoại' },
      { field: 'email', header: 'Email' },
      { field: 'ghiChu', header: 'Ghi Chú' },
    ];
  }

  inHoaDon() {
    var hoadon = new jsPDF();
    hoadon.text(85, 10, 'Trung tam Tieng Anh');
    hoadon.text(90, 20, 'Hoa Don Hoc phi');
    hoadon.text(10, 50, 'Ten hoc sinh: Nguyen Thi Thuy');
    hoadon.text(10, 60, 'Ten lop: Xi Tin');
    hoadon.text(10, 70, 'Hoc phi: 1000$');
    hoadon.text(10, 80, 'So buoi hoc: 8');
    hoadon.text(10, 90, 'Ngay dau khoa: 01/05/2019');
    hoadon.text(10, 100, 'Ngay cuoi khoa: 10/05/2019');
    hoadon.text(10, 130, 'Ha Noi, ngay...,thang...,nam...');
    hoadon.text(10, 140, 'Chu ki nguoi nop tien'); hoadon.text(140, 140, 'Chu ki nguoi thu tien');
    hoadon.save("hoadon.pdf");
  }
  them() {
    this.newGroup = true;
    this.group = {};
    this.displayDialog = true;
  }
  save() {
    let groups = [...this.groups];
    console.log(this.group);
    if (this.newGroup)
      groups.push(this.group);
    else
      groups[this.groups.indexOf(this.selectedgroup)] = this.group;

    this.groups = groups;
    this.group = null;
    this.displayDialog = false;
    window.alert('Lưu Thành Công')
    
  }
  delete() {
    let index = this.groups.indexOf(this.selectedgroup);
    console.log(index);
    this.groups = this.groups.filter((val, i) => i != index);
    this.group = null;
    this.displayDialog = false;
  }
  sua() {
      this.newGroup = false;
      // this.group = this.cloneCar(event.data);
      this.displayDialog = true;

    }

  onRowSelect(event) {
    this.newGroup = false;
    this.group = this.cloneCar(event.data);
    this.select = true;
    this.displayDialog = true;
  }
  xoa() {
    if ( this.select) {
      let index = this.groups.indexOf(this.selectedgroup);
      console.log(index);
      this.groups = this.groups.filter((val, i) => i != index);
      this.group = null;
      this.displayDialog = false;
      this.select = false;
    }
    else{
      window.alert('Vui Lòng Chọn Bác Sĩ')
    }

  }
  cloneCar(c: Group): Group {
    let group = {};
    for (let prop in c) {
      group[prop] = c[prop];
    }
    return group;
  }

}
