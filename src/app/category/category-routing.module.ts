import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClinicServiceComponent } from './clinic-service/clinic-service.component';

const routes: Routes = [
  {
    path: 'clinic-service', component: ClinicServiceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule { }
