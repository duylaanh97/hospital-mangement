import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Group } from '../../user/group/model/group.model';


@Injectable()
export class CarService {

    constructor(private http: HttpClient) {}

    getgroup() {
        return this.http.get<any>('assets/demo/data/group.json')
                    .toPromise()
                    .then(res => <Group[]> res.data)
                    .then(data => data);
    }
}
