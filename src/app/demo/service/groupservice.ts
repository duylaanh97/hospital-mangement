import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Group } from 'src/app/user/group/model/group.model';


@Injectable()
export class GroupService {

    constructor(private http: HttpClient) {}

    getgroup() {
        return this.http.get<any>('assets/demo/data/group.json')
                    .toPromise()
                    .then(res => <Group[]> res.data)
                    .then(data => data);
    }
}
