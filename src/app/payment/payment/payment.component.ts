import { Component, OnInit } from '@angular/core';
import { Payment } from './model/payment.model';
import { PayService } from 'src/app/demo/service/payservice';
import { MessageService } from 'primeng/primeng';


@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css'],
  providers:[PayService]
})
export class PaymentComponent implements OnInit {

  payment: Payment = {}

  payments: Payment[];

  cols: any[];

  cols2: any[];

  cols3: any[];

  displayDialog: boolean;

  displayDialogls: boolean;

  displayDialogadd: boolean;

  newPayment: boolean;

  selectedpayment: Payment;

  select : boolean = false;

  constructor(private paymentService: PayService, private messageService: MessageService) { }

  ngOnInit() {
    this.paymentService.getpayment().then(payments => {
      console.log(payments);
      this.payments = payments;
  });

  
  this.cols = [
    { field: 'stt',  header: 'STT' },
    { field: 'hoTen', header: 'Họ Tên' },
    { field: 'soDienThoai', header: 'SĐT' },
    { field: 'chiTiet', header: 'Chi Tiết Khám Chữa' },
    { field: 'dichVu', header: 'Dịch vụ đã sử dụng' },
    { field: 'bacSiChinh', header: 'Bác Sĩ Chính' },
    { field: 'bacSiPhu', header: 'Bác Sĩ Phụ' },
    { field: 'phiDichVu', header: 'Phí Dịch Vụ' },
    { field: 'phiDaThanhToan', header: 'Phí Đã Thanh Toán' },
    { field: 'phiChuaThanhToan', header: 'Phí Chưa Thanh Toán' },
    { field: 'hinhThucThanhToan', header: 'Hình Thức Thanh Toán' }
  ];

  this.cols2 = [
    { field: 'stt', header: 'STT'},
    { field: 'ngayKham', header: 'Ngày Khám Chữa'},
    { field: 'chiTiet', header: 'Chi Tiết Khám Chữa'},
    { field: 'dichVu', header: 'Dịch vụ đã sử dụng'},
    { field: 'bacSiChinh', header: 'Bác Sĩ Chính'},
    { field: 'bacSiPhu', header: 'Bác Sĩ Phụ'},
    { field: 'phiDichVu', header: 'Tổng Chi Phí'},
    { field: 'phiDaThanhToan', header: 'Phí Đã Thanh Toán'},
    { field: 'phiChuaThanhToan', header: 'Phí Chưa Thanh Toán'}
  ];

  this.cols3 = [
    { field: 'stt', header: 'STT'},
    { field: 'dichVu', header: 'Dịch Vụ'},
    { field: 'donGia', header: 'Đơn Giá'},
    { field: 'soLuong', header: 'Số Lượng'},
    { field: '', header: 'Thành Tiền'},
    { field: '', header: 'Bác Sĩ Chính'},
    { field: '', header: 'Bác Sĩ Phụ'}
  ]
}

them() {
  this.newPayment = true;
  this.payment = {};
  this.displayDialogadd = true;
}
xem(){
  this.displayDialogls = true;
}
save() {
  let payments = [...this.payments];
  if (this.newPayment)
    payments.push(this.payment);
  else
    payments[this.payments.indexOf(this.selectedpayment)] = this.payment;

  this.payments = payments;
  this.payment = null;
  this.displayDialog = false;
  window.alert('Lưu Thành Công')
  
}
delete() {
  let index = this.payments.indexOf(this.selectedpayment);
  console.log(index);
  this.payments = this.payments.filter((val, i) => i != index);
  this.payment = null;
  this.displayDialog = false;
}
sua() {
    this.newPayment = false;
    // this.payment = this.cloneCar(event.data);
    this.displayDialog = true;

  }

onRowSelect(event) {
  this.newPayment = false;
  this.payment = this.cloneCar(event.data);
  this.select = true;
  this.displayDialog = true;
}
xoa() {
  if ( this.select) {
    let index = this.payments.indexOf(this.selectedpayment);
    console.log(index);
    this.payments = this.payments.filter((val, i) => i != index);
    this.payment = null;
    this.displayDialog = false;
    this.select = false;
  }
  else{
    window.alert('Vui Lòng Chọn Bác Sĩ')
  }

}
cloneCar(c: Payment): Payment {
  let payment = {};
  for (let prop in c) {
    payment[prop] = c[prop];
  }
  return payment;
}


}
