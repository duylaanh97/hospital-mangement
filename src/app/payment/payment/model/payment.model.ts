export class Payment {
    constructor(
        stt: string,
        hoTen: string,
        soDienThoai: string,
        chiTiet: string,
        dichVu: string,
        bacSiChinh: string,
        bacSiPhu: string,
        phiDichVu: string,
        phiDaThanhToan: string,
        phiChuaThanhToan: string,
        hinhThucThanhToan: string,
        ngayKham: string,
        donGia: string,
        soLuong : string,
        
    ){}
}